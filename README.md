# NTA Diagrams

## Docker local development setup

You should have docker installed in your system, if not click [here](https://docs.docker.com/get-docker/).

1. Go to diagrams root directory.

2. Retrieve all custom icons.

    ```shell
    docker-compose run --rm diagrams python ./icons/retrieve.py
    ```

3. Generate all diagrams.

    ```shell
    docker-compose run --rm diagrams ./run.sh
    ```

4. Generate a single diagram.

    ```shell
    docker-compose run --rm diagrams python ./diagrams/web_service.py
    ```
