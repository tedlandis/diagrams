#!/bin/bash
set -e

DIAGRAMS=./diagrams/*.py
for diagram_path in $DIAGRAMS
do
    diagram=$(basename $diagram_path .py)
    echo "Processing $diagram ..."
    rm -f ./output/${diagram}.png
    python ${diagram_path}
done
