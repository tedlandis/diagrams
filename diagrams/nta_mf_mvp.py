from diagrams import Cluster, Diagram, Edge
from diagrams.onprem.ci import Jenkins
from diagrams.onprem.container import Docker
from diagrams.onprem.queue import RabbitMQ
from diagrams.onprem.network import Nginx
from diagrams.k8s.compute import Pod
from diagrams.elastic.elasticsearch import Elasticsearch, Kibana, LogStash
from diagrams.programming.language import Python


with Diagram(
    name="NTA Metrics Framework MVP", filename="output/nta_mf_mvp", show=False
):
    with Cluster("CICD"):
        ci_cd = Jenkins()
        test_run = Docker("Test Run")
    ci_cd >> test_run
    store = Nginx("Store")
    queue = RabbitMQ("Queue")
    test_run >> Edge(label="1 - Artifacts", color="darkgreen") >> store
    test_run >> Edge(label="2 - Event", color="brown") >> queue

    transformer = Pod("XML -> JSON")

    queue >> transformer << store

    metrics_repo = Elasticsearch("Metrics Repo")

    with Cluster("Metrics Reporting"):
        reporting = [Kibana("Dashboard"), Kibana("Report")]

    transformer >> LogStash() >> metrics_repo >> reporting

    with Cluster("xATS VM", graph_attr={"bgcolor": "LemonChiffon"}):
        results = LogStash("Results")
        Python("ROBOT") >> results

    results >> Edge(label="", color="darkgreen", style="dashed") >> store
    results >> Edge(label="", color="brown", style="dashed") >> queue
