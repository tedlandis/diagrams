import diagrams
from diagrams.aws import compute, database, network


with diagrams.Diagram("Web Service", filename="output/web_service", show=False):
    network.ELB("my_lb") >> compute.EC2("my_web") >> database.RDS("my_deb")
