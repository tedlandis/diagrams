FROM python

RUN apt-get update \
    && apt-get install -y --no-install-recommends graphviz \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/diagrams

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
